# simh-infoserver150vxt-VAXVMS073: Simulation of DEC InfoServer 150VXT providing OpenVMS VAX v7.3 installation CD-ROM

This is a pre-configured [SIMH](https://github.com/simh/simh) instance to simulate a [Digital Equipment Corporation (DEC)](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) InfoServer 150VXT system, serving the OpenVMS VAX v7.3 installation CD-ROM over a LAN. This simulation can be used for booting a real or simulated MicroVAX system over a network, and fresh installation of OpenVMS v7.3 onto it. This uses non-routable protocols such as [MOP](https://en.wikipedia.org/wiki/Maintenance_Operations_Protocol) and LAD, so it only works when the InfoServer emulation and any of its clients are on the same LAN.

**This repository does not include any OpenVMS Product Authorization Keys (PAKs).** You can install OpenVMS v7.3 on a real or simulated MicroVAX over a LAN with the simulation instances in this repository, but you will need to obtain valid PAKs to actually use the installed system. The author of this repository cannot provide PAKs.

This simulation was set up using the InfoServer software provided on the OpenVMS Freeware v80 CD-ROM, which may be found [here](https://www.digiater.nl/openvms/freeware/v80/infoserver/). I have also archived InfoServer software and documentation for convenience [here](https://gitlab.com/NF6X_VAXen/InfoServer-docs), and OpenVMS 7.3 documentation [here](https://gitlab.com/NF6X_VAXen/OpenVMS-v7.3-docs).


## System Configuration

Description            | Default setting
-----------------------|------------------
Server name            | IS_VAXVMS073
Administrator password | ess
Network interface      | vde:/var/run/vde2/tap1.ctl


## Using the Simulation

Before you can run this simulation instance, you will need to download and install the [SIMH](https://github.com/simh/simh) software, particularly the `infoserver150vxt` simulator. You will probably need to edit the initialization file provided here to configure networking for your host system. The initialization file assumes that you will be using a [Virtual Distributed Ethernet](https://github.com/virtualsquare/vde-2) switch with its control port located at `/var/run/vde2/tap1.ctl`. Depending on your host operating system, you may need to use a different network configuration. SIMH network configuration is described in the [0readme_ethernet.txt](https://github.com/simh/simh/blob/master/0readme_ethernet.txt) file provided with SIMH.

To launch the simulation:

   infoserver150vxt IS_VAXVMS073.ini

To shut it down, log in with the administrator password, either on the console or via [LAT](https://en.wikipedia.org/wiki/Local_Area_Transport). Then use the `shutdown` command.

You might find it helpful to run the simulation in a detached `screen` session. The included script `start-screen.sh` starts the simulator with its console in a `screen` session, with the `screen` escape key set to `^p` so that it does not conflict with SIMH's use of `^e` as its escape key.

An included DCL script shows how to mount disks from the InfoServer simulation under OpenVMS.

## File Manifest

Filename                     | Description
-----------------------------|--------------------------------------------
IS_VAXVMS073.ini             | SIMH `infoserver150vxt` initialization file
IS_VAXVMS073.nvr             | InfoServer nonvolatile RAM image
IS_VAXVMS073_system_rz1.rz24 | InfoServer system disk image
MOUNT_VAXVMS073.COM          | Example DCL script to mount CD-ROMs from InfoServer
OpenVMS_VAX_073_OS.iso       | OpenVMS VAX v7.3 installation CD-ROM image
README.md                    | This file
start-screen.sh              | Example shell script to launch simulator in a detachable `screen` session
